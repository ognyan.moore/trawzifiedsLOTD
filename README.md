# trawzifieds Legacy of the Dragonborn

Based off the Lexy's Legacy of the Dragonborn modlist, this list aims to expand on it while cranking up the visuals to eleven and containing popular community requested mods fitting my vision of Skyrim.

**Current modlist status: Up and running**

[Gameplay Overview (WIP)](GameplayGuide.md)

![trawzifieds LOTD Header Image](Pictures/0.9.8.png)

<!-- vim-markdown-toc GitLab -->

* [Recommended PC Specifications](#recommended-pc-specifications)
* [Installation](#installation)
	* [Step 1 - Preparing the Wabbajack installation environment](#step-1-preparing-the-wabbajack-installation-environment)
	* [Step 2 - Downloading Wabbajack](#step-2-downloading-wabbajack)
	* [Step 3 - Downloading the modlist](#step-3-downloading-the-modlist)
	* [Step 4 - Selecting the list through Wabbajack](#step-4-selecting-the-list-through-wabbajack)
	* [Step 5 - Selecting the installation folder](#step-5-selecting-the-installation-folder)
	* [Step 6 - Installing the modlist](#step-6-installing-the-modlist)
	* [Step 7 - Optimizing Skyrim on NVIDIA GPUs](#step-7-optimizing-skyrim-on-nvidia-gpus)
	* [Step 8 - Installing ENB](#step-8-installing-enb)
	* [Step 9 - Moving Game Folder Files](#step-9-moving-game-folder-files)
	* [Step 10 - Starting Skyrim](#step-10-starting-skyrim)
* [MCM Settings](#mcm-settings)
	* [3PCO - 3rd Person Camera Overhaul](#3pco-3rd-person-camera-overhaul)
	* [A Matter of Time](#a-matter-of-time)
	* [All Geared Up Deriv.](#all-geared-up-deriv)
	* [Cathedral Weather](#cathedral-weather)
	* [CLARALUX : Lighting](#claralux-lighting)
	* [Complete Alchemy](#complete-alchemy)
	* [Complete Crafting](#complete-crafting)
	* [Deadly Dragons](#deadly-dragons)
	* [Diverse Dragons Col. 3](#diverse-dragons-col-3)
	* [EasyWheel](#easywheel)
	* [Enhanced Blood](#enhanced-blood)
	* [ESF: Companions](#esf-companions)
	* [Expanded Towns](#expanded-towns)
	* [Extended UI](#extended-ui)
	* [Farmhouse Chimneys](#farmhouse-chimneys)
	* [Follower Framework](#follower-framework)
	* [Honed Metal](#honed-metal)
	* [Immersive Armors](#immersive-armors)
	* [iWant RND Widgets](#iwant-rnd-widgets)
	* [Keep it Clean](#keep-it-clean)
	* [Less Intrusive HUD](#less-intrusive-hud)
	* [Lock Overhaul](#lock-overhaul)
	* [moreHUD](#morehud)
	* [Not So Fast MG](#not-so-fast-mg)
	* [Not So Fast MQ](#not-so-fast-mq)
	* [OBIS - Patrols](#obis-patrols)
	* [PC Head Tracking](#pc-head-tracking)
	* [Predator Vision](#predator-vision)
	* [Quick Light](#quick-light)
	* [Realistic Needs](#realistic-needs)
	* [Realistic Water Two](#realistic-water-two)
	* [SkyUI](#skyui)
	* [SSoB](#ssob)
	* [Storm Lightning](#storm-lightning)
	* [The Ultimate Dodge Mod](#the-ultimate-dodge-mod)
	* [Thieves Guild Req.](#thieves-guild-req)
	* [Timing is Everything](#timing-is-everything)
	* [Trade & Barter](#trade-barter)
	* [Ultimate Combat](#ultimate-combat)
	* [VioLens](#violens)
	* [Widget Addon](#widget-addon)
	* [Wildcat Combat](#wildcat-combat)
* [Frequently Asked Questions](#frequently-asked-questions)
	* [I have a 21:9 monitor. Does this modlist work with it?](#i-have-a-219-monitor-does-this-modlist-work-with-it)
	* [Is there a way to easily make the game more family friendly regarding nudity?](#is-there-a-way-to-easily-make-the-game-more-family-friendly-regarding-nudity)
	* [Can my PC run this modlist?](#can-my-pc-run-this-modlist)
	* [What Settings Can I Adjust To Improve Graphical Performance](#what-settings-can-i-adjust-to-improve-graphical-performance)
	* [How Can I Re-enable Pausing in Menus?](#how-can-i-re-enable-pausing-in-menus)
	* [I want to add this mod. Can you help me with it?](#i-want-to-add-this-mod-can-you-help-me-with-it)
	* [Is there a way to disable the character switching in dialogue?](#is-there-a-way-to-disable-the-character-switching-in-dialogue)
	* [Can I run the modlist in different languages?](#can-i-run-the-modlist-in-different-languages)
	* [I'm getting screen tearing. What's the best way to go about fixing that?](#im-getting-screen-tearing-whats-the-best-way-to-go-about-fixing-that)
* [Credits](#credits)

<!-- vim-markdown-toc -->

## Recommended PC Specifications

Estimate for 1080p @ 60fps.

**GPU** GTX 1070 / RX 580 (8GB of VRAM is HIGHLY recommended for a stutter-free experience)	

**CPU** Intel i7-4770 / AMD Ryzen 5 1600

**RAM** 16GB of DDR3, DDR4 preferred (users below 16GB may experience crashes)	

**Storage**
- Modlist Size: 158GB
- Downloads Size: 86GB
- Total Size: **244GB**

## Installation

### Step 1 - Preparing the Wabbajack installation environment

Create a Wabbajack folder and an installation folder on the root of your drive.

Example:

`C:\Wabbajack` (Wabbajack folder)	
`C:\trawzifiedsLOTD` (Installation folder, where Mod Organizer will be installed)

### Step 2 - Downloading Wabbajack

Get [the latest Wabbajack build](https://www.github.com/wabbajack-tools/wabbajack/releases) from GitHub (the executable file named `Wabbajack.exe`).

Place it in the Wabbajack folder.

### Step 3 - Downloading the modlist

~~Download the latest version of the modlist installer here.~~

The Wabbajack CDN can no longer serve files your browser understands and can download. Thus, you'll have to start Wabbajack and click on the console window at the top right.

Enter the following: `wabbajack-cli.exe download-url -u https://wabbajack.b-cdn.net/trawzifieds%20LOTD%200.9.8.1.wabbajack_15fb6cda-538a-407a-accd-bf59533e5833 -o trawzifiedsLOTD0.9.8.1.wabbajack`

The file will download inside of the version directory (currently called 2.0.4.4).

This is basically a set of instructions Wabbajack will use to replicate my installation on your computer.

### Step 4 - Selecting the list through Wabbajack

Open up Wabbajack from your Wabbajack folder.

Select *Install from Disk* and choose the `.wabbajack` file you downloaded in step 3.	

### Step 5 - Selecting the installation folder

Select your installation folder in Wabbajack and point it to the folder created in step 1.	

Downloads will be automatically stored in a subdirectory (`C:\trawzifiedsLOTD\downloads`), but if you would like to save space on the installation drive, create an empty Downloads folder on the drive you wish to keep them on and select that instead.

### Step 6 - Installing the modlist

Click the little play button in Wabbajack to start installing the modlist!

If you get an error with a specific mod while installing, that mod likely updated and removed its old files so the modlist installer needs an update.

Feel free to notify me about this in the [Wabbajack Discord](https://discord.gg/ZHXVmTs), if you haven't seen anyone else mention it yet and it isn't pinned.

If you don't like Discord, you can always reach me on here (GitLab), [GitHub](https://github.com/tr4wzified) or even via [Mastodon](https://www.fosstodon.org/@trawzified).

### Step 7 - Optimizing Skyrim on NVIDIA GPUs

If you have an NVIDIA GPU I recommend performing the following steps. Skip this if you're using an AMD card.

* Open `nvidiaProfileInspector.exe` which can be found in `<your installation folder>\Tools\NVIDIA Profile Inspector\`.
* At the top, click the little arrow pointing down to a disk.
* You should get a little dropdown, one of them saying `Import profile(s)`. Click it.
![NVIDIA Profile Inspector Icon](Pictures/README/ImportingNVIDIAProfile.png)
* Select `trawzifieds LOTD.nip` and open it. It's in the same folder you launched the Profile Inspector from.
* You should see 'Profile succesfully imported!'. Click OK and after that hit Apply at the top right.
* Close NVIDIA Profile Inspector.

### Step 8 - Installing ENB

This modlist is designed to use with an ENB. I personally use Serio's ENB, I think it's the best one out there out of the ones I've tested.	
To install ENB, first head over to [the ENBSeries website](http://enbdev.com/download_mod_tesskyrimse.htm) and at the bottom, select the latest ENB version (v0.415 as of writing this).

On the next page, click the download button all the way at the bottom.	
Now open up the downloaded zip archive, go to WrapperVersion and drag **ONLY** `d3d11.dll` and `d3dcompiler_46e.dll` into your Skyrim game folder.

If you don't know where that is, right-click The Elder Scrolls V: Skyrim Special Edition in your Steam Library, select properties, go to the 'Local Files' tab and click on 'Browse Local Files'.

Now go to [Serio's ENB mod page](https://www.nexusmods.com/skyrimspecialedition/mods/30506?tab=files) and download the file manually.
Open it up with a program like [7Zip](https://www.7-zip.org/), go into the folder called 'Serio's ENB - Cathedral' and copy all the files into your Skyrim game folder.	

Proceed by downloading my modified ENB settings. They can be downloaded [here](https://wabbajackpush.b-cdn.net/trawzifieds%20ENB%20Settings-94370606-b22e-4770-ac89-dcadd37cf6c3.7z).

Simply extract the files again, and move all the files into the game folder	Overwrite when asked. If you don't get an overwrite prompt, you might have done it wrong.

### Step 9 - Moving Game Folder Files

Once your installation is complete, proceed to go to your Skyrim game directory.

Copy all the files inside the Game Folder Files directory (in the installation directory) into your Skyrim game directory.

### Step 10 - Starting Skyrim

Almost there!

Simply open up Mod Organizer in the installation directory (`ModOrganizer.exe`), wait for it to load, select SKSE from the dropdown on the top right and click 'Launch'.	This will open up Skyrim. Start a new game and once you're loaded in, wait for all the messages to stop flowing in.	This might take about a minute.

Afterwards, continue with the Mod Configuration Menu settings below.

## MCM Settings

Press Escape and select 'Mod Configuration Menus' to start with this section.

### 3PCO - 3rd Person Camera Overhaul
**General Settings**
* Min zoom: 130
* Max zoom: 300

### A Matter of Time
**Presets**
- Load user settings: Go

Feel free to customize this however you want. I prefer a small 24 hour clock in the bottom right.

### All Geared Up Deriv.
**Weapons - Player**

* Display Options
  * Shield stays on arm while Equipped: enabled

### Cathedral Weather
**Settings**

* Settings
  * Load Settings

### CLARALUX : Lighting
**General**
* LIGHT Settings
	* Presets: MyGoodEye's Dark Nights
* ADDITIONAL LIGHT Enablers *(it's on the top right)*:
	* PATH TORCHES: disabled

### Complete Alchemy

**Cooking**
* Water
  * Craft Water: disabled
  * Get Water from Wells: disabled

**Help**
* Troubleshooting
  * Ragdoll Paralysis: disabled

### Complete Crafting
**Recipe Display**
* Crafting Menu Filters
  * Breakdown Recipes: enabled
  
**Crafting Options**
* Additional Items
  * Artifact Replicas: enabled
  * Matching Set Circlets: enabled
  
**Learning & XP**
* Smithing Experience
  * Tanning Rack: 0.1
  * Smelter: 0.2
  * Mining: 20
* Learn to Craft
  * Learning Points required to Craft: 250
  
**Mining & Materials**
* Mining
  * Mining Presets: Faster Mining
* Firewood
  * Firewood per chop: 6
  * Max per activation: 1

### Deadly Dragons
**Dragons**
* Presets
  *  Difficulty: Expert
* Special
  * Knockdown: disabled

### Diverse Dragons Col. 3
**Dragons**
- Nether Dragon: disabled
- Sanguine Dragon: disabled
- Vile Dragon: disabled

Close the MCM, wait for Diverse Dragons to do its thing, it'll show a message on the top left. Continue.

### EasyWheel

**Available Functions**
* Show/Hide Functions
  * Dovahkiin's Relax: Hidden
  * Simple Action: Hidden
  * Hearthfire Multi Kid Adoption: Hidden

**Configuration**
* Wheel Layout
  * 8: Pray
* Wheel Keycode: whatever you prefer, just don't forget about it :) I use K.

### Enhanced Blood
**Screen Blood**
- Enabled: disabled

### ESF: Companions
**Requirements**
* Requirements Before Your Trial 
  * Player Level: 30
* Requirements Before Joining The Circle
  * Player Level: 40
* Requirements Before Kodlak's Request 
  * Player Level: 60

### Expanded Towns
**Settings**
* Fortification Walls 
  * Dawnstar: disabled
  * Falkreath: disabled
  * Morthal: disabled
  * Winterhold: disabled

### Extended UI
* Stats Menu 
  * Hide Legendary UI elements: enabled
  * Show attribute modifiers: enabled
  * Show skill modifiers: enabled

### Farmhouse Chimneys
**Mod Support**

* Arthmoor Village Options *Enable everything but Falkreath, as shown below (don't forget to scroll down to Whistling Mine)*
  * Darkwater Crossing: enabled
  * Dawnstar: enabled
  * Dragon Bridge: enabled
  * Falkreath: **DISABLED**
  * Ivarstead: enabled
  * Karthwasten: enabled
  * Keld-Nar: enabled
  * Kynesgrove: enabled
  * Rorikstead: enabled
  * Shor's Stone: enabled
  * Soljund's Sinkhole: enabled
  * Whistling Mine: enabled
* New Villages
  * Helarchen Creek: enabled
* Misc Mods
  * Cutting Room Floor: enabled
* Mod Compatibility
  * Expanded Towns and Cities: enabled

### Follower Framework

**System**
* Save/Load Configuration
  * Load from File

### Honed Metal

* General
  * Crafting Cost Multiplier: 0.85
  * Tempering Cost Multiplier: 0.35
  * Enchanting Cost Multiplier: 0.75
  * Recharge Cost Multiplier: 0.85
  * Materials Cost Multiplier: 1.10
* Maintenance 
  * Crafting Time: 0.5
  * Enchanting Time: 0.5
  * Recharging Time: 0.0
  * Common Materials Acquisition Time: 0.02

### Immersive Armors
**Armor Options**
* Crafting
  * Akaviri Samurai Armor: disabled
  * Alduin Scale Armor: disabled
  * Barbarian Hero Armour set: disabled
  * Boiled Chitin Armor: disabled
  * Daedric Lord Armor: disabled
  * Falkreath Armor: disabled
  * Redguard Knight Mail: disabled
  * Witchplate Armor: disabled
* Distribution
  * Akaviri Samurai Armor: disabled
  * Alduin Scale Armor: disabled
  * Barbarian Hero Armour set: disabled
  * Boiled Chitin Armor: disabled
  * Daedric Lord Armor: disabled
  * Falkreath Armor: disabled
  * Redguard Knight Mail: disabled
  * Witchplate Armor: disabled

**EXIT THE MCM. Wait until the message 'Immersive Armors Configuration Has Finished' appears. Continue.**

### iWant RND Widgets

* User Load/Save
    * Load: GO!

### Keep it Clean
**Settings**
* Toggles
  * Start Keep It Clean: enabled

**EXIT THE MCM. Wait until the message 'Keep it Clean started' appears. Continue.**

### Less Intrusive HUD

**General Settings**
* MOD Configuration
  * Load Preset: enabled

**EXIT THE MCM. Wait for Less Intrusive HUD to complete, it's like 5 seconds. Continue.**

**HUD Visibility**

* HUD Items Visibility
  * Show Alternate Crosshair: enabled
  * Show Alternate Sneak Crosshair: disabled
* Compass Markers
  * Show Undiscovered Locations: enabled
  * Show Discovered Locations: enabled
  * Show Enemy Markers: **DISABLED**
  * Show Player Set Marker: enabled
  * Show Floating Markers: enabled
  * Show Quest Markers: enabled

### Lock Overhaul

**General**
- Activate Lock Overhaul

**EXIT THE MCM. Wait for Lock Overhaul to activate, it's like 5 seconds. Go back to Lock Overhaul.**

* General Settings
  * Allow Increasing Skill - enabled
  * Enable the sound effect - enabled
  * Enabled Crime - enabled
  * Allow Auto Open When Unlocked - **DISABLED**

**Smash Locks**
* Activate Smash Locks - enabled
* Allowed Weapons - Two + One Handed

### moreHUD

**Presets**
Save setting with PapyrusUtil
- Preset: trawzifieds LOTD
- Load User Settings?: Go

**EXIT THE MCM. Wait for moreHUD to activate, it's like 5 seconds. Go back to the MCMs.**

### Not So Fast MG

* Minimum Days Before Events
  * Saarthal Expedition: 3
  * Psijic Monk Visit: 7
  * Brelyna's Practice: 4
  * J'Zargo's Experiment: 4
  * Onmund's Request: 4

### Not So Fast MQ

* Minimum Days Before Events
  * First Dragon Sighting: 3
  * Note From Delphine: 6
* OTHER
  * No Negotiations: enabled

### OBIS - Patrols

**Settings**

* Bandit Patrols
  * Enable?: enabled
* Difficulty
  * How Tough?: Toughest

### PC Head Tracking

Preferences
- Load Settings: TRAWZIFIEDSLOTD

Optionally you can enable a custom voice type for your character here.

### Predator Vision

* Keys
  * Nightvision activation key: V (or whatever you prefer)
  * Predator vision activation key: H (or whatever you prefer)
* Settings
  * Nightvision Color: 30%
  * Predator Vision Color Boost: 70%

### Quick Light

- Brightness: Bright
- Enable to Long press activation key: disabled

### Realistic Needs
**Basic Needs**
* Start RN&D

* Auto-Reminder
    * Sound Notification: disabled

**Toggles**
* First Person Messages: enabled

### Realistic Water Two
**Mod Options**

Blacksmith Forge Water
- The Fall of Granite Hill: enabled
- Kynesgrove: enabled
- Rorikstead: enabled
- Open Cities: **DISABLED**
- Expanded Towns and Cities: enabled

### SkyUI
**General**

* Item List
  * Font Size: Small (whatever you prefer)
  * Category Icon Theme: Celtic, by GreatClone (whatever you prefer)
* Active Effects HUD
  * Icon Size: Small (whatever you prefer)

**Controls**

Set up your favorite groups here. I suggest binding the rest of the F1-F12 keys and changing Quick Save in the Skyrim Controls settings to something else.

**Advanced**

SWF Version Checking
- Disable all.

### SSoB

- Quest Markers on Stones of Barenziah: enabled

### Storm Lightning

**Presets**

* Load Preset
  * Ultra Realistic

**Settings**
* Fork Lightning
  * Min Fork Distance: 1

### The Ultimate Dodge Mod

This is an odd one.

First, configure your dodge key **NOT** by going to the MCM, but go to the Controls tab in the System menu (where you enter MCM) and set your Sneak control binding to the dodge key you want to use.

Now go to the MCM page again.

* General Settings
  * Combat Dodging Style: Sidestep only
  * Lock Default Dodging Style: enabled
  *Dodging Style Toggle Key: NumLock (or whatever key you're NEVER going to press - the roll is not meant to be used, only sidestep!)
* Player Settings
  * Sneak Key: Ctrl (or whatever you normally use for Sneak)
* NPC Settings
  * NPC Dodge AI: disabled

### Thieves Guild Req.
**Misc Options**

* User Presets
  * Load Preset

**Select 'Load Preset' TWICE. For some reason not all settings apply correctly the first time.**

### Timing is Everything

**Extra Options**
* Presets
  * Load Preset

**Select 'Load Preset' twice if you don't see anything changing, happens from time to time.

### Trade & Barter

**Barter Rates**

* Settings
  * Modify Barter Setting: enabled
* Presets
  * Barter Presets: Medium

### Ultimate Combat

**General**

* Timed Block
  * Effective Time: 0.00s
  * Blur Strength: 0.0s
* Game Balance Settings
  * Speed Bonus: disabled
  * Player Killmove Immunity: **ENABLED**
* Others
  * Swing Effect: disabled
* Stagger
  * Enemy Pose: disabled
  * Player Stagger: disabled
  * NPC's Bow Poise: '0.00
  * Player Bow Poise: '0.00s
* Locational Damage
  * Headshot Damage Mult: 0.0
  * Headshot Message: disabled
  * Locational Damage Sound: disabled
  * Locational Damage Effect: disabled

**NPC Settings**

* NPC
  * Dodge: Middle
* Giant
  * HP Mult: 1.0
* Dwarven Centurion
  * HP Mult: 1.0
* Dragon Priest
  * HP Mult: 1.0

### VioLens

**Profile System**

* Menu Settings
  * Load: trawzifieds LOTD

### Widget Addon

**Main**

* General Settings
  * Load User Settings?: Do it!
 

### Wildcat Combat

* Injuries
  * Disable Injuries: enabled

---

You're ready to start the game!	

You might get a popup about Moonlight Tales - choose 'No Scaling'.	

You might also get a popup about Wintersun - choose your preferred deity!

**Have fun!**

## Frequently Asked Questions

### I have a 21:9 monitor. Does this modlist work with it?

It does, but it requires a few additonal steps. Once the modlist is installed, open up Mod Organizer and look at the left pane.	

Search for '21x9' in the search bar at the bottom right of the left pane.
Uncheck the bottom two mods with **Disable me for 21x9 support** in the name (assuming you're sorting by priority). These are called the following:

- Better Dialogue Controls - Disable me for 21x9 support
- Dear Diary - Disable me for 21x9 support

### Is there a way to easily make the game more family friendly regarding nudity?

You can switch profiles in Mod Organizer (drop down menu at the top) and select trawzifieds LOTD SE - NeverNude instead.

### Can my PC run this modlist?

This modlist was designed with higher end machines in mind. Make sure to check the recommended system requirements up top.
Personally, I run this on the following rig:

* **CPU:** Intel Core i7 4790K (OCd @ 4.6GHz) w/ Cooler Master Hyper 212EVO
* **GPU:** Gigabyte GeForce RTX 2080 Super Gaming OC @ +60 Core Clock & +1200 Memory Clock
* **RAM:** Kingston HyperX Beast 16GB (OCd @ 2000MHz)
* **SSD:** Kingstom A400 256GB
* **Monitor:** Dell S2417DG (2560x1440)

I get around 50-90 FPS outdoors, and 90-150 inside of buildings and such.

### I want to add this mod. Can you help me with it?

I can't offer you support for mods you added yourself. Always check for conflicts with other mods in xEdit.

You are free to request it if it hasn't been suggested before (check the issues tab here: https://gitlab.com/trawzified/trawzifiedsLOTD/issues), and perhaps I'll look into it if I like it.

### Is there a way to disable the character switching in dialogue?

There is, you can disable it by searching for `Alternate Conversation Camera` in Mod Organizer, double click it.

Go to the INI Files tab, select the ini and set `bSwitchTarget` from `1` to `0`.

### Can I run the modlist in different languages?

No, it will cause a ton of inconsistencies with the other mods, they do not all have a translation.

### I'm getting screen tearing. What's the best way to go about fixing that?

Personally, I use G-Sync so I don't use any sort of VSync. The best way to enable VSync is to search for `SSE Display Tweaks` in Mod Organizer, double click it and go to the `INI Files` tab. Click `SKSE\Plugins\SSEDisplayTweaks.ini`. Now look for the option saying `EnableVSync=false`, should be around line 45. Change `false` to `true`.

### Which settings can I adjust to improve graphical performance?

* In `SkyrimCustom.ini` (from Mod Organizer -> Wrench/Screwdriver Icon at the top -> INI Editor)
    * Change `iMinGrassSize` from `70` to `90`. You can further gain performance by incrementing this in steps of 10, but I'd stop at 120 - not much more to gain after that.
* In `SkyrimPrefs.ini` (from Mod Organizer -> Wrench/Screwdriver Icon at the top -> INI Editor)
    * Change `iShadowMapResolution` from `2048` to `1024`
* ENB Settings (in-game hit Shift + Enter)
  * Uncheck Ambient Occlusion, click Save Configuration on the top

 
If you still need more frames, you could turn off ENB.

Of course you could turn off other stuff, but that is beyond my support for your install. Don't change more if you don't know exactly what you're doing.

You will have to redo these settings when updating the modlist via Wabbajack.

### Can I re-enable pausing in menus?

You can enable it by searching for `Skyrim Souls RE - Updated` in Mod Organizer, double click it.

Select the `INI Files` tab, click on `SKSE\Plugins\SkyrimSoulsRE.ini`.

In the `[UNPAUSED_MENUS]` section, change the `true` values to `false` for the menus you want to be paused.

Save by either pressing the save icon above the editor, or by pressing Ctrl + S.

## Credits

**DarkLadyLexy** for creating the incredible list this modlist has originally been built off: Lexy's Legacy of the Dragonborn.	

**Mdc211** for getting me into Wabbajack in the first place, and this fork is actually based off his Wabbajack modlist. Wabbaception!

**halgari**, **erri120**, **Noggog** and others for developing Wabbajack.	

**All the other awesome people in the Wabbajack community!**
