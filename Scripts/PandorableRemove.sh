#!/bin/bash
# Run me in the Pandorable NPCs mod folder!
# For Lexy's Legacy of the Dragonborn
rm Meshes/Actors/Character/FaceGenData/FaceGeom/Skyrim.esm/0001C187.NIF
rm Meshes/Actors/Character/FaceGenData/FaceGeom/Skyrim.esm/00013BB0.NIF
rm Meshes/Actors/Character/FaceGenData/FaceGeom/Skyrim.esm/00013265.NIF
rm Meshes/Actors/Character/FaceGenData/FaceGeom/Skyrim.esm/00013269.NIF
rm Meshes/Actors/Character/FaceGenData/FaceGeom/Skyrim.esm/00013271.NIF
rm Meshes/Actors/Character/FaceGenData/FaceGeom/Skyrim.esm/0001C197.NIF
rm Meshes/Actors/Character/FaceGenData/FaceGeom/Skyrim.esm/0001C184.NIF
rm Meshes/Actors/Character/FaceGenData/FaceGeom/Skyrim.esm/0001412D.NIF
rm Meshes/Actors/Character/FaceGenData/FaceGeom/Skyrim.esm/0001412C.NIF
rm Meshes/Actors/Character/FaceGenData/FaceGeom/Skyrim.esm/00014123.NIF
rm Meshes/Actors/Character/FaceGenData/FaceGeom/Skyrim.esm/0001336F.NIF
rm Meshes/Actors/Character/FaceGenData/FaceGeom/Skyrim.esm/00013B9B.NIF
rm Meshes/Actors/Character/FaceGenData/FaceGeom/Skyrim.esm/0001329F.NIF
rm Meshes/Actors/Character/FaceGenData/FaceGeom/Skyrim.esm/00013476.NIF
rm Meshes/Actors/Character/FaceGenData/FaceGeom/Skyrim.esm/00014121.NIF
rm Meshes/Actors/Character/FaceGenData/FaceGeom/Skyrim.esm/0001337C.NIF
rm Meshes/Actors/Character/FaceGenData/FaceGeom/Skyrim.esm/0001C189.NIF
rm Meshes/Actors/Character/FaceGenData/FaceGeom/Skyrim.esm/000132AE.NIF

rm textures/actors/character/FaceGenData/FaceTint/Skyrim.esm/0001C187.dds
rm textures/actors/character/FaceGenData/FaceTint/Skyrim.esm/00013BB0.dds
rm textures/actors/character/FaceGenData/FaceTint/Skyrim.esm/00013265.dds
rm textures/actors/character/FaceGenData/FaceTint/Skyrim.esm/00013269.dds
rm textures/actors/character/FaceGenData/FaceTint/Skyrim.esm/00013271.dds
rm textures/actors/character/FaceGenData/FaceTint/Skyrim.esm/0001C197.dds
rm textures/actors/character/FaceGenData/FaceTint/Skyrim.esm/0001C184.dds
rm textures/actors/character/FaceGenData/FaceTint/Skyrim.esm/0001412D.dds
rm textures/actors/character/FaceGenData/FaceTint/Skyrim.esm/0001412C.dds
rm textures/actors/character/FaceGenData/FaceTint/Skyrim.esm/00014123.dds
rm textures/actors/character/FaceGenData/FaceTint/Skyrim.esm/0001336F.dds
rm textures/actors/character/FaceGenData/FaceTint/Skyrim.esm/00013B9B.dds
rm textures/actors/character/FaceGenData/FaceTint/Skyrim.esm/0001329F.dds
rm textures/actors/character/FaceGenData/FaceTint/Skyrim.esm/00013476.dds
rm textures/actors/character/FaceGenData/FaceTint/Skyrim.esm/00014121.dds
rm textures/actors/character/FaceGenData/FaceTint/Skyrim.esm/0001337C.dds
rm textures/actors/character/FaceGenData/FaceTint/Skyrim.esm/0001C189.dds
rm textures/actors/character/FaceGenData/FaceTint/Skyrim.esm/000132AE.dds

rm PAN_NPCs.esp
