# Changelog

## [UNRELEASED] 0.9.8.2

**Removed** Campfire and Frostfall - Unofficial SSE Update (Reason: this mod wasn't doing anything, forgot to remove it during Frostfall removal)

---

**Fixed** Nether's Follower Framework not working (scripts being overridden) with Interesting NPCs

**Fixed** SFCO painting clipping with Distinct Interiors Painting in Radiant Raiment - disabled DI painting

**Fixed** Added Nilsine Shatter-Shield back to FollowerFaction, but added to NFF nwsFFImportExclude so the dialogue doesn't show up

**Fixed** LotD followers are no longer importable, following Lexy

**Fixed** Battlemages may no longer get two iron helmets in their inventory, same with steel swords.

**Fixed** SkyRem - Eve not applying correctly to Katla, Dravin Llanith, Pavo Attius, Skaggi Scar-Face, Beitild, Leigelf, Indara and a bunch of other miners

**Fixed** A couple of JaySuS Leveled Lists from the CCOR Patches Merge being overwritten by NPC Retexture Merge

**Fixed** Aranea Ienith not having Steel Dagger of Azura instead of Iron Dagger

**Fixed** Crash when getting near Helarchen Creek - removed WiZKiDs Helarchen Creek patch, could not find an issue with the nif

**Fixed** Farmhouse05.nif from Vivid Landscapes Parallax has been hidden - caused Parallax Bug to appear on roofs

---

**Changed** Bears' health has been buffed to 250 (was 200), Stamina increased to 300 (was 240)

**Changed** Horses' health has been buffed to 600 (was 297), Stamina increased to 300 (was 238), Level increased to 15 (was 10), Confidence changed to Cowardly (was Average)

**Changed** Uthgerd the Unbroken now uses Opposite Gender Anims

**Changed** Renamed a few horse names I felt were cheesy

**Changed** Snow grass is no longer a bunch of ferns, should look much better

## 0.9.8.1

You can safely update to this version from 0.9.8.

**Removed** Coverkhajiits (Reason: this mod has been replaced fully by the other Khajiit retextures)

---

**Updated** SSE Engine Fixes (5.2 -> 5.2 (they reuploaded the file with different ini settings I think?))

**Updated** SSE Display Tweaks (0.3.14 -> 0.4.0)

**Updated** Papyrus Compiler App (4.0.0 beta 7 -> 4.0.0 beta 8)

**Updated** NVIDIA Profile Inspector (2.3.0.2 -> 2.3.0.12)

---

**Fixed** You will no longer crouch down while in RaceMenu - I've adjusted The Ultimate Dodge Mods' MCM to set sneak to CTRL by default.

**Fixed** You should be able to import the NVIDIA profile now with the newer version of the inspector.

---

**Changed** Reran TexGen and DynDOLOD - forgot to set TexGen back to 512 from 1024.

**Changed** No longer using WABBAJACK_INCLUDE on the LOD files - this makes the file much smaller and is much faster to install, but it requires some more effort on my part.

**Changed** Automated parts of the Ultimate Combat MCM

## 0.9.8 - The Unholy Crusade Update

A new save is required for this update, like with all major (0.9.x) updates.

*A special thank you to **j9ac9k** for helping me with the README formatting and **Cypren** for debugging some issues in the list.*

**Added** VIGILANT SE

**Added** VIGILANT Voiced - English Addon

**Added** VIGILANT - NPC Overhaul

**Added** VIGILANT Armors ReTexture SE

**Added** Better Dynamic Snow Patches (Cutting Room Floor, Lucien, VIGILANT)

**Added** New RaceMenu Preset! (Khajiit, Male)

**Added** iWant RND for CACO

**Added** iWant RND Widgets

**Added** Jagged Crown - Replacer (Desaturated)

**Added** WiZKiD Signs

**Added** OMEGA AIO Updated

**Added** JaySuS Swords

**Added** Lexy's CCOR Consistency Patch

**Added** Immersive Sounds Compendium CCOR Patch

**Added** Rally's Tel Mithryn 4K + 512 LODs

**Added** Rally's Raven Rock 4K

**Added** Rally's Riekling Outposts 4K

**Added** True Wolves of Skyrim 2K (+ SkyTEST Patch)

**Added** AIM FIX - total control over the crosshair for archery and magic (+ iHUD Patch)

**Added** Better Container Controls for SkyUI

~~**Added** Unreal 4K-8K Tents ReTexture~~ Creator has hidden the file on the Nexus. I'll add it if it comes back up.

**Added** RUSTIC AMULETS - Special Edition

**Added** Talisman of Treachery HD

**Added** Talos Amulet SD - High Poly SE (meshes only)

**Added** Widget Addon - Keep it Clean

**Added** SSE-SRO - Enhanced Spidersac 4K Brown

**Added** Parallax Pelts

**Added** 4K SMIM Furniture Chest

**Added** 4K Carts Rustic

**Added** High poly HD Rabbit by Pfuscher

**Added** 4K SMIM Carriage Seat

**Added** RUDL - Removing Unpleasant Details from Logs

**Added** Extraordinary Eggs SE

**Added** Masks of the Dovah Sonaak SE

**Added** Medieval Spirits

**Added** Masculine Khajiit Textures (Grey Cat and Leopard)

**Added** Vampire Facial Reclamation

**Added** The Eyes of Beauty - Player

**Added** The Eyes of Beauty - Elves

**Added** The Eyes of Beauty - NPCs

**Added** Bounty Preview

**Added** Hand to Hand Blocking Animation

**Added** Rally's Glowing Mushrooms

**Added** Rally's Lava 4K

**Added** Rally's Solstheim Plants - Creep Cluster 4K

**Added** Rally's Solstheim Plants - Mushrooms Lods 512

**Added** Rally's Solstheim Plants 4K

**Added** A Guiding Light - Clairvoyance Reimagined

**Added** Miraak's Rustic Hermetic Robes

**Added** Feral - Claw Unarmed Attacks for Beast Races - Go for the Throat

**Added** Necro Archer - Bound Arrow Reanimation

**Added** Dawnguard Arsenal SSE

**Added** Better Bears

**Added** JS Barenziah SE - 4K Textures

**Added** Frankly HD Dragonbone and Dragonscale - Armor and Weapons - 4K

**Added** Dragon Priest Weapons Improved

**Added** Quiet Dog

**Added** Project Clarity - Vanilla Armor Textures Redone (Note: removed all textures that were already overwritten)

**Added** Rally's Solstheim Landscapes

**Added** Predator Vision - Alternate Start Fix

**Added** White Lighthouse

**Added** Sleep Tight SE

**Added** Sleep Tight SE Fixes

**Added** Supreme Dwemer Spheres

**Added** New RaceMenu Preset! (Dunmer, Male)

**Added** M'rissi Tails of Troubles Replacer

**Added** KG's M'rissi Retexture - CBBE - 3BBB - UNP - TMBE (using UNP)

**Added** Inigo Player Marriage Commentary Patch for M'rissi

**Added** Skyrim Disable Fast Travel

**Added** Papyrus Compiler App

---

**Removed** OMEGA AIO (Reason: Switched to OMEGA Updated AIO, an updated version of the mod by another author)

**Removed** Immersive Weapons (Reason: Most of these are pretty low quality or have weird animations, and Lexy stopped maintaining patches for these)

**Removed** Real Rabbits HD (Reason: replaced with Pfuschers new model)

**Removed** Windhelm Lighthouse (Reason: replaced with White Lighthouse)

**Removed** Bandolier - Bags and Pouches (Reason: This had conflicts with ALLGUD and clipping issues with other armors I can't be bothered to resolve and they were horribly balanced. I feel like the backpacks from Campfire already add enough carryweight, these bags don't look very spectacular.)

**Removed** kryptopyr's Patch Hub - Bandolier CCOR Patch (Reason: see above, don't need this anymore)

**Removed** CCOR Bandoliers patch fix (Reason: why am I even typing this one again)

---

**Updated** SSE Display Tweaks (0.3.6 -> 0.3.14)

**Updated** Serana Dialogue Add-on (0.92 -> 1.0)

**Updated** Legacy of the Dragonborn (5.1.0 -> 5.1.1)

**Updated** Better Dynamic Majestic Mountains (6.0 -> 9.0)

**Updated** Majestic Mountains (2.90 -> 3.10)

**Updated** 360 Movement Behavior SE (1.3.1 -> 1.3.2)

**Updated** Snazzy's Ancient Dwemer Display Cases (1.7 -> 1.7.1)

**Updated** CBPC - CBP Physics with Collision (1.3.5 -> 1.3.6)

**Updated** Dear Diary (2.1 -> 2.2.1)

**Updated** KS Hairdos SMP (1.2 -> 1.3)

**Updated** Halted Stream Mine (1.2.1 -> 1.3)

**Updated** Improved College Entry - Questline Tweaks (2.5 -> 2.6)

**Updated** DynDOLOD Resources (2.81 -> 2.82)

**Updated** Nether's Follower Framework (2.6.5 -> 2.6.6)

**Updated** Wrye Bash (307 Beta 5 -> 307 Beta 6.1)

**Updated** Improved College Entry (2.6 -> 2.7)

---

**Fixed** An issue where the effects from CACO weren't applied correctly to health potions

**Fixed** The wrong script being attached to No Stone Unturned, markers should now appear correctly

**Fixed** Black Hands Dagger having the wrong first person model

**Fixed** You won't be able to craft infinite amounts of Nethers' Follower Tokens anymore.

**Fixed** Your character never wanting to stop mining again, [diggy diggy hole](https://www.youtube.com/watch?v=34CZjsEI1yU)

**Fixed** Small conflict between Campfire and Immersive Children resulting in children not being able to chop wood

**Fixed** Parallax bug appearing on meshes/architecture/whiterun/wrclutter/WRBrazier01.nif

**Fixed** Bugged navigation mesh near Darkwater Crossing bridge (Immersive Hold Borders)

**Fixed** Disabled pausing on the lockpick menu because of compatibility issues with Ordinator

**Fixed** Clipping issues with the yellow mountain flowers in the East Exhibit Halls of LotD

**Fixed** M'rissi's Tails of Troubles not being set to NeverNude in the NeverNude version of the list

---

**Changed** Inigo and Auri's dialogue to each other should fire a little less frequently. Priority set to 30 (was 50).

**Changed** Skyrim Unique Treasures' automated MCM now includes Immersive Ownership and the Notifcation Message has been disabled

**Changed** Greatly increased the visual fidelity on the main menu

**Changed** Various Distinct Interiors paintings have been replaced

**Changed** Difficulty levels in the starting cell have been ordered from easy to hard. Necromancer start has changed from Very Hard to Impossible. If you play this start and manage to get out of Blackreach, send me your save and you'll get credits for one free extra mod suggestion™

**Changed** Note: VIGILANT is startable from level 40 and on, Maelstrom is now startable from level 20 and on.

**Changed** FOV for your hands/spells/weapons should now be identical to your actual FOV (90).

**Changed** LOD resolution has been reduced back to Lexy's settings (was LOTD+). This should help with stuttering in exteriors, and I don't notice a difference.

## 0.9.7.10

**Added** Frankly HD Dragon Bones 8K-4K

---

**Removed** RUSTIC DRAGON CORPSE 4K (Reason: switching to Frankly HD Dragon Bones because it both looks better and this archive was having some issues during installation)

## 0.9.7.9

**Fixed** A case where the installer could fail to extract RUSTIC Dragon Corpse files.

## 0.9.7.8

**Updated** Nether's Follower Framework (2.6.4 -> 2.6.5)

## 0.9.7.7

**Updated** Mountain Flowers by Mari SE 2K (1.2 -> 1.1) (different Nexus Page)

## 0.9.7.6

A new save is required for this update.

Note: This modlist cannot be installed with Wabbajack versions below 2.0.

---

**Added** Lexy's M'rissi Experience Patch (makes it so that you get more XP for the Tails of Trouble quests)

**Added** SSE Display Tweaks

---

**Removed** aMidianBorn Imperial Armor and Studded (Reason: these textures are obsolete due to Frankly HD Imperial Armor and Weapons overwriting)

**Removed** SKSE64 Havok Fix (Reason: replaced by SSE Display Tweaks)

---

**Updated** Lexy's LOTD Special Edition Consistency Patches - Weapons and Armor Merge (1.18 -> 1.02) (??)

**Updated** unofficial performance optimized textures AKA (UPOT) (previously known as Performance Textures Optimized SSE) (8.0 -> 9.0)

**Updated** Legacy of the Dragonborn Patches (Official) (1.8.1 -> 1.9)

**Updated** Serana Dialogue Add-On (0.86 -> 0.92)

**Updated** The Flying Knight (2.0.2 -> 2.0.4)

**Updated** Nether's Follower Framework (2.6.3 -> 2.6.4)

**Updated** Unofficial Skyrim Special Edition Patch (4.2.2 -> 4.2.3)

---

**Fixed** The Scout capes not being visible (edited mesh and esp to only use slot 40).

**Fixed** You'll no longer be able to access the deleted and buggy Alternate Starts by using the random option - it has been removed.

**Fixed** The ELE-ELFX consistency patch breaking the Thalmor Embassy in combination with Lexy's ELFX steps - ELFX esp has been restored to the original.

**Fixed** The Nightingale hood will now be visible on more races, but it's still invisible on Argonians, Khajiit and Orcs due to clipping issues.

**Fixed** A clipping issue with a wolf and a Distinct Interiors painting in Haelgas Bunkhouse.

**Fixed** Distinct Interiors placing an object in front of the Keep it Clean door in the Sleeping Giant inn which made it really hard to find for new players

---

**Changed** The night will now be from 19:00 to 6:00 for vampires (was 19:00 to 5:00). Thanks **DarkladyLexy** for the patch!

**Changed** LOTD Patches Merge now includes patches for JK's Arcadia, Belethor, Warmaidens, Dragonsreach and Skyrim Sewers.

**Changed** Adjusted some MCM settings in the README, automated a few of them with FISS/PapyrusUtil (NFF, PC Head Tracking).

## 0.9.7.5

**I highly recommend upgrading to this release if you were using previous releases.**

A new save is required for this update.

**Removed** powerofthree's Papyrus Extender for SSE (Reason: this is a leftover from back when I used to have Frozen Electrocuted Combustion in the list, and is no longer used for anything)

**Removed** HQ Tree Bark (Reason: experimenting with Tree LODs trying to find the cause of the two lods for one tree issue)

---

**Updated** Honed Metal Voiced (1.3 -> 1.4)

**Updated** ELE-ELFX Consistency Patch and Tweaks (1.1 -> 1.2)

**Updated** Interesting NPCs SE (4.3.11 Reupload -> 4.3.12)

**Updated** Nether's Follower Framework (2.6.2 -> 2.6.4)

**Updated** Inn Rooms Costs (3.1 -> 4.0.1)

**Updated** JK's Warmaidens (1.0 -> 1.1)

**Updated** Serana Dialogue Add-on (0.85hotfix -> 0.86)

**Updated** Better Jumping (1.6.8 -> 1.7.1)

---

**Fixed** A critical issue affecting Nord and child races, applying the wrong effects and keywords. Thanks **IronmanKenlin** for alerting me about this.

**Fixed** Rudy HQ - Miscellaneous SE - Imperial Candles not being enabled on the NeverNude profile

**Fixed** Serana turning into the Headless Horseman

**Fixed** Some Guards Armor Replacer records were inproperly smashed by Mator Smash

**Fixed** The Guards Armor Replacer LotD patch not being included

**Fixed** wrscafstr01spikes.nif and wrwallchunktower01.nif having the Parallax bug (caused by Illustrious Whiterun)

## 0.9.7.4

**Updated** RUSTIC WINDOWS - Special Edition - 2K (1.0 -> 2.0)

---

**Fixed** A potential issue with Bleakwind Bluff the Bashed Patch put errors into - proper record has been forwarded manually

**Fixed** Re-enabled Rudy HQ - Miscellaneous SE - Imperial Candles. This mod was previously in 0.9.6, but I disabled it during debugging of the 0.9.7 release. It has been re-included.

## 0.9.7.3

**Updated** Interesting NPCs SE (4.3.11 -> 4.3.11 Reupload)

**Updated** Project Dova-Hen - Alternate Chicken (1.1 -> 1.2)

---

**Fixed** Parallax bug (white textures) on farmhouse lods and the farmhouse wells
## 0.9.7.2

**Updated** Performance Optimized Textures for SSE (6.0 -> 7.0)

**Updated** Ragged Flagon Sign 4K 2K (2.2 -> 2.3Parallax)

**Updated** Interesting NPCs SE (4.3.9 -> 4.3.11)

**Updated** SSE Engine Fixes (5.1.1 -> 5.2.0)

**Updated** Blended Roads (1.6 -> 1.7)

**Updated** Scaleform Translation Plus Plus (1.4 -> 1.4.1)

---

**Fixed** Locked the maximum FPS to 240 (through HavokFix), should prevent infinite loading screens and increase loading speeds

**Fixed** Forgot about Thieves Guild Requiements MCM configuration - added now in a preset

**Fixed** Serana having multiple leveling scripts

**Fixed** The Flying Knight having another seam in the landscape, thanks **[FF]TheRogueleeder**!

**Fixed** The Flying Knight having black faces - temporarily downgraded to 2.0.2 from 2.0.3 to fix this issue

**Fixed** Predator Vision not working - my customized ENB files have been rebuilt from the ground up with the new Serio ENB version

**Fixed** Depth of Field was completely disabled in the Skyrim ini files causing underwater effects to look very odd and clear

---

**Changed** Inigo and his summoning friend have been slightly nerfed. Inigo won't have an Ebony Bow anymore.

~~**Changed** Inigo and Lucien now have the NFF Ignore Token by default, which removes the Import dialogue that breaks them. Do not remove this out of their inventory!~~

**Changed** Inigo and Lucien can no longer be imported, as this breaks them. Thanks **johanlh** for the cleaner solution with the FormID list!


## 0.9.7.1

A new save is required. People had sorting table issues when going from LotD 5.0.32 to 5.1.0.

**Updated** Legacy of the Dragonborn SSE (5.0.32 -> 5.1.0)

**Updated** Serana Dialogue Add-On (0.85 -> 0.85hotfix)

**Updated** Honed Metal Voiced (1.1 -> 1.2)

---

**Fixed** Leftover files in Game Folder Files

**Fixed** ALLGUD xEdit Scripts should now correctly download in the installer

**Fixed** Grass Fixes Merge plugins not being hidden (shouldn't affect anything from looking weird)

**Fixed** SKSE not being selected by default (is that even a fix?)

## 0.9.7 - The Quality Questing Update
**Added** High Quality Tintmasks for Cutting Room Floor

**Added** High Quality Tintmasks for Various Mods - Interesting NPCs (Cuyima overwriting)

**Added** High Quality Tintmasks for Various Mods - Relationship Dialogue Overhaul

**Added** 4K HQ Puddles - No Ripples version

**Added** Night Mummy - A Night Mother Makeover (2K)

**Added** Immersive Citizens Extended - JK's Whiterun

**Added** JK's Arcadia's Cauldron

**Added** Misc Retexture Project

**Added** Distinct Interiors (I have personally changed all of the NSFW paintings!)

**Added** Distinct Interiors Fixes

**Added** JK's Belethor's General Goods

**Added** Vivid Landscapes - Orc and Farmhouses Parallax (4K)

**Added** Vivid Landscapes - Orc and Farmhouses Parallax (4K) - ELFX Patch

**Added** Renthal Nettle SSE

**Added** JS Shrines of the Divines SE

**Added** Rally's Wall of Kings (Frozen)

**Added** Solstheim mushrooms by Mari

**Added** Dave's Lilypads

**Added** Serana Dialogue Addon

**Added** Undead summons emerge from the ground

**Added** Skyrim 3D Trees and Plants - Plants only (I have handpicked only a few)

**Added** HD Meshes and Textures for Animal and Creature Drops

**Added** Bullish Bovine

**Added** JK's Warmaidens

**Added** JK's Dragonsreach

**Added** Dynamic Animation Replacer

**Added** EVG Conditional Idles

**Added** Taarie's Dialogue Fix

**Added** Spooky Philter of the Phantom

**Added** Haunting and Mourning - SSE

**Added** Welcome Back to the Bee and Barb

**Added** Respect for the Arch-Mage

**Added** A Good Death - Old Orc's Various Opponents

**Added** Arniel's Quest Speed-up

**Added** Arniel's Quest Speed-up - Even Better Quest Objectives Patch

**Added** Improved College Entry - Questline Tweaks

**Added** Thugs Not Assassins

**Added** Thugs Not Assassins - Timing is Everything

**Added** Solitude and Temple Frescoes 2019 - Solitude Only

**Added** Thieves Guild Requirements SE - FISS version

**Added** All Thieves Guild Jobs Concurrently

**Added** Elven Weapons for Silence SE

**Added** Elven Weapons for Silence SE - 4K Dark Obsidian Textures

**Added** Finding Derkeethus

**Added** Finding Helgi and Laelette

**Added** Majestic Mountain Textures for High Poly Project

**Added** Finding Susanna Alive

**Added** Glow Be Gone SKSE Updated (FleshFX & weapon glow is kept enabled)

**Added** Better Honey Nut Treats

**Added** DOOR (Puzzle door dust effect replacer)

**Added** MIST (retexture for the mist effects)

**Added** Sigils of Skyrim

**Added** Simple Belly Paints SE - Bodypaints of Shadowmarks Skillpaints and Faction Paints - RaceMenu Overlays

**Added** Lavender by Mari 4K (converted with CAO)

**Added** Aevrigheim - Miraak's Sword and Staff Replacer (Variant 2)

**Added** Honed Metal Voiced

**Added** New RaceMenu Preset! (Orc, Male)

---
**Removed** BetterFalmerCaveCeilingGlow (Reason: mod obsolete due to HD Falmer Armor and All Falmer Things Retexture already doing a better job)

**Removed** Legacy of the Dragonborn Fix (Reason: this one was a temporary fix by me for LotDs Dome crash, which has been resolved in the latest update)

**Removed** Go to Bed (Reason: this mod has incompatibilities with The Ultimate Dodge Mod if you enter a bed while sneaking)

**Removed** Noldorian Auriels Shield (Reason: this mod was already being completely overridden by Remiros' Auri-El HD Redone and thus obsolete)

**Removed** CTD On Death Alas (Reason: while this mod takes good care of the issue of people reloading their saves, it's too frustrating for people, looking into other death mods)

**Removed** No Edge Glow - Magic and Transformations (Reason: mod replaced by Glow Be Gone SKSE Updated)

**Removed** Designs of the Nords (Reason: this mod has been replaced by Sigils of Skyrim, from the same author)

---
**Updated** 360 Movement Behavior SE (1.3 -> 1.3.1)

**Updated** SkyRem - CORI (5.0.3 -> 5.0.4)

**Updated** Glorious Doors of Skyrim (GDOS) (1.02 -> 1.04)

**Updated** moreHUD SE (3.6.4 -> 3.7.6)

**Updated** Cathedral Weathers and Seasons (2.091 -> 2.22)

**Updated** SkyRem - EVE (2.0 -> 2.1.1)

**Updated** SSE Engine Fixes (SKSE64 Plugin) (4.13.0 -> 5.1.1)

**Updated** Majestic Mountains - Darkside (2.82 -> 2.90)

**Updated** All Geared Up Derivative xEdit Scripts (1.8.3.2 -> 1.8.3.3)

**Updated** Lexy's Experience ini (1.07 -> 1.08) (Thieves Guild exp is now less with Thieves Guild mods)

**Updated** .NET Script Framework (12.0 -> 13.0)

**Updated** Nether's Follower Framework (2.6.1 -> 2.6.2)

**Updated** High Poly Project (4.8 -> 4.9)

**Updated** Better Dynamic Snow (2.10 -> 2.10.1)

**Updated** Volumetric Mists (1.4.0 -> 1.4.1)

**Updated** Even Better Quest Objectives (1.8.3beta -> 1.8.4)

**Updated** LeanWolf's Better-Shaped Weapons SE (2.0.13 -> 2.1.03)

**Updated** Lucien - Fully Voiced Follower (1.4.3b -> 1.5.0)

**Updated** The Flying Knight (2.0.2 -> 2.0.3)

**Updated** Rally's Dark Elf Furniture (1.0 -> 1.1, also switched to 4K)

---
**Fixed** The guards of Winterhold, Reach, Hjaalmarch, Stormcloak Reach, Riften, Dawnstar, Winterhold, Stormcloaks, Siege Stormcloaks, Ralof, Ulfric + Jod (Dawnstar) wearing a mix of different armors

**Fixed** The brighter museum fix I provided not applying once again. Damn you, automated patchers!

**Fixed** The Pelagius Wing no longer has two level indicators in the name.

**Fixed** The Hall of Rumination, Derelict Pumphouse, Debate Hall, War Quarters (all in Blackreach) now properly play Yggdrasils Music.

**Fixed** Sinderion's Field Laboratory now uses ELE lighting.

**Fixed** The Frostflow Lighthouse no longer has two level indicators in the name. Actually not my fault for once, small mistake that was probably fixed already in newer versions of Lexy's patches.

**Fixed** ENB Fire will no longer make your graphics card beg for mercy. Additionally, the weird lighting on the roads has been fixed and the light is brighter. (The fix mostly was disabling BigRange)

**Fixed** The Flying Knight is no longer located under the ground.

**Fixed** Horses now use the appropriate models and will no longer have invisible manes.

**Fixed** Guards walking around with the wrong helmet on.

**Fixed** Audio Overhaul for Skyrim SE not applying correct sounds in various regions due to changed weather.

**Fixed** Master files being cleaned incorrectly (and Apocrypha crashing) due to xEdit having a critical bug with the cache. Thanks Lively and Umgak for reporting this!

**Fixed** Floating lantern from CLARALUX in the Whiterun shack near the Giant fight

**Fixed** Particle lights no longer suck your FPS down from 110 to 30.

**Fixed** The floating lantern from CLARALUX in the Whiterun shack near the Giant fight has been removed.

**Fixed** Typos in the quest stages when you start the College of Winterhold questline.

**Fixed** Nilsine Shatter-Shield being seen as a follower (thank you Civitas!)

---
**Changed** Lethal Traps are less lethal. To be more precise, I now mostly use Lethal Traps values * 0.55. This is much more than vanilla but doesn't instakill you.

**Changed** Traps no longer stagger you. I don't like stunlocks in games.

**Changed** trawzifiedsYggdrasilResolution.esp has been forwarded to my SMASH Override.

**Changed** SeranaFix.esp has been forwarded to my SMASH Override.

**Changed** trawzifiedsLOTDFix has been included in ELE Merged.esp

**Changed** The museum should now be even brighter.

**Changed** USSEP will no longer give you a random book upon spawning.

**Changed** Moonlight Tales will no longer give you a ring upon spawning.

**Changed** OBIS will no longer give you a book upon spawning.

**Changed** True 360 Movement is now merged into Pre Bash Merged.esp

**Changed** Auri will no longer give you a Summon Auri spell when meeting her.

**Changed** Diverse Dragons will no longer have a menu in your spells if you didn't do MCM. Which you should have done!

**Changed** Equipped items will now show up on the top of your SkyUI inventory

**Changed** Now using the ported version of Better Bats instead of the original LE version

**Changed** Now using Rally's Desaturated Hanging Moss instead of the original

## 0.9.6.4

**It should be safe to update from 0.9.6.2. Back up your saves, located in <installation folder>/profiles/trawzifieds LOTD SE Nude (or NeverNude if you play on that)/saves and copy them back in after the update.**

**Fixed** The nude profile not being fully updated to 0.9.6.3

---
**Changed** 21:9 users now have even better support, the dialogue won't fall off the screen and the messageboxes will look like intended. Check the FAQ for more information.

## 0.9.6.3

**It should be safe to update from 0.9.6.2. Back up your saves, located in <installation folder>/profiles/trawzifieds LOTD SE Nude (or NeverNude if you play on that)/saves and copy them back in after the update.**

**Updated** 360 Movement Behavior SE (1.2 -> 1.3)

**Updated** Smile in HD (1.0 -> 1.2)

---
**Fixed** About 30 water seams (especially around Morthal / Falkreath) mainly caused by Animals Merged.esp loading after Realistic Water Two. Occlusion regenerated.

---
**Changed** Game Folder Files are now handled manually by me instead of Wabbajack and should contain less useless stuff.

## 0.9.6.2

**A new save is required for this update.**

**Updated** Pandorable's NPCs (1.4 -> 1.3) (Reason: I have downgraded to 1.3 for now to fix the black faces. I need to set up the NPC Retexture Merge profile that Lexy uses in order to properly update in 0.9.7.

---

**Fixed** Serana having a double perk from Know Your Armor (Armor Perk 2)

**Fixed** Several black face bugs (Endarie, Saffir, Gerda, Fianna)

**Fixed** Immersive Patrols Lite 2.2.3 having 192 spawn markers for non existent NPCs

**Fixed** 3 Bashed Patch enchantment errors

**Fixed** .NET Script Framework not being properly installed. Make sure to redo the steps with Game Folder Files for this update.

**Fixed** The Dunmer Alternate Starts should have a difficulty assigned to them now.

**Fixed** It's no longer possible to use the vampire start in Alternate Start. Previously, the start would make you both a Sacrosanct and vanilla vampire at the same time + the enemies could become aggressive easily when reentering the cell you spawned in.

**Fixed** Bashed Patch overriding my Alternate Start difficulty indicators. Solution: remove all dialogue topics from Bashed Patch in the future.

**Fixed** Bashed Patch overriding a ton of armors and changing the name of them back to vanilla for no reason?

**Fixed** Bashed Patch messing up a ton of leveled lists, such as the guards wearing both original and Guards Armor Replacer armor or mudcrabs carrying gold.

**Fixed** Bashed Patch messing up a ton of weapons and restoring them to vanilla values in some cases.

**Fixed** The Keep it Clean YOT patch should no longer contain typos.

**Changed** The bad writing in the journals and books from Immersive Hold Borders has been replaced with my own. Feedback is welcome!

**Changed** The bathroom fee has increased from a mere 10 septims to 60.

## 0.9.6.1

**Fixed** The Legacy of the Dragonborn Museum lighting fix not being applied correctly

**Fixed** Auri having two of the same exact perks applied to her. I'm aware that the antlers still seem to be on her.

**Fixed** Modlist name accidentally set to 'trawzifieds LOTD SE' instead of 'trawzifieds LOTD'

## 0.9.6 - The Freaky Fashion Update

**Added** All Geared Up Derivative

**Added** HQ Solitude - 4K Blue Palace Floor

**Added** HQ Solitude - Stonewall

**Added** Vanilla Table Replacers (4K-2K)

**Added** Maelstrom - Fully Voiced Follower and Quest Mod SSE

**Added** Maelstrom - Fully Voiced Follower and Quest Mod SSE - 4K Ran Textures

**Added** Better Dwemer Exteriors SSE

**Added** Better Dwemer Exteriors SSE - No Snow Under The Roof Patch (included in the NSUTR Merge)

**Added** Eye Normal Map Fix SSE (being overridden by Improved Eyes Skyrim, only beast eyes are used)

**Added** Vanilla Table Replacers 4K 2K

**Added** Mikan Eyes - SE (Converted to ESL)

**Added** Hirsute Beards (Converted to ESL)

**Added** Beast Hair Horn and Beard - Vanilla based - Hair Replacer for Khajiit and Argonian (Standalone)

**Added** Improved Rain and Fog for Cathedral Weathers - Darker Nights (Level 1 Lighting)

**Added** Guards Armor Replacer SSE

**Added** Bug Fixes SSE

**Added** kryptopyr's Patch Hub - Wintersun TCIY Patch (included in Wintersun - Merge.esp)

**Added** New Beginnings - Live Another Life Extension SSE

**Added** Address Library for SKSE Plugins

**Added** Improved Names Customized SSE (using RomanNumeral)

**Added** Serana's Tomb Blood Curse (included in Miscellaneous Merged.esp)

**Added** Rally's Hanging Moss

**Added** Giant Lichen by Mari (4K option) (converted with CAO)

**Added** Forgotten Plants Textured (2K gleamblossom + glowing ground plant) (converted with CAO)

**Added** Finally First Person Magic Animation (variant 2 FPMA 6.0) (converted with CAO)

**Added** Scathecraw 2K (converted with CAO)

**Added** 360 Movement Behavior SE

**Added** Mum's the Word

**Added** CCOR Bandoliers patch fix (included in CCOR Patches Merged.esp)

**Added** LEACM (Landscape Environment and Clutter Merged) Consistency Patch (included in Landscape Environment and Clutter Merged.esp)

**Added** New RaceMenu preset! (Altmer, Female)

---
**Removed** Noldorian Dawnbreaker (Reason: this texture has been made obsolete by Dawnbreaker Redone 2, which I prefer)

**Removed** Noldorian Auriels Shield (Reason: this texture has been made obsolete by Remiros' Auri-El HD Redone, which I prefer)

**Removed** Rally's Solstheim Scathecraw 2K (Reason: replaced with Scathecraw 2K)

---
**Updated** Ragged Flagon Sign 4k 2k (2.1 -> 2.2)

**Updated** SkyRem - CORI (5.0.2 -> 5.0.3)

**Updated** Andromeda - Standing Stones of Skyrim (1.1.1 -> 1.1.2)

**Updated** Mystic Condenser (UCE) (2.2 -> 2.3)

**Updated** --JawZ-- Northern Lights Weather Add On for Cathedral Weathers (1.1 -> 1.4)

**Updated** Pandorable's NPCs (1.3 -> 1.4)

**Updated** Cathedral Weathers and Seasons (2.08 -> 2.091)

**Updated** Improved Rain and Fog for Cathedral Weathers (3.2 -> 3.2.1)

**Updated** Fix Note icon for SkyUI (SKSE64 Plugin) (1.2 -> 1.2.1)

**Updated** kryptopyr's Patch Hub - Ordinator CCOR Patch (1.1 -> 1.2)

**Updated** Rally's Hooks and Saws (1.0 -> 1.0 ENV Rusty)

**Updated** ConsoleUtilSSE (1.0.5 -> 1.2.0)

**Updated** I'm Walkin' Here (1.2.1 -> 1.4.0)

**Updated** Scaleform Translation Plus Plus (1.2 -> 1.4.0)

**Updated** Stay At The System Page - Updated (1.3.1 -> 1.5.0)

**Updated** Whose Quest Is It Anyway (1.1.0 -> 1.3.0)

**Updated** Yes I'm Sure (1.3 -> 1.5.0)

**Updated** Nether's Follower Framework (2.6.0 -> 2.6.1)

--- 
**Fixed** Ilas-Tei & Bandit Dropper (38000D55) had a black face bug

**Fixed** trawzifiedsDiverseWerewolvesResolution.esp in the Animals Merge depended on the ELE Merge, which is a no no and messed up load order resulting in various cells not having ELE lighting.

**Fixed** trawzifiedsCustomResolution.esp contained incorrect reference to Black Hands Dagger first person model which could result in a crash

**Fixed** Legacy of the Dragonborn museum being a bit too dark

---
**Changed** ECHOs Dark Brotherhood Shrouded Armour for UNP SSE version now overrides TMB Vanilla Armors and Clothes SSE

**Changed** Bashed Patch now uses NAME tag on OBIS SE.esp

**Changed** Morthal now has much higher chance of foggy weather

**Changed** Cathedral Weathers is now set at Sombre in the MCM by default (this also enables Obsidian Weathers from Improved Rain & Fog)

**Changed** Timing is Everything now needs to be set up manually, it deviates slightly from Lexy's in some areas because I felt like it fit better.

**Changed** Removed Auri's Antlers. I don't think she liked me going at it with the chainsaw.

**Changed** The main menu music has been changed from Heilung - In Maidjan to Nytt Land - Ragnarok

**Changed** The two mods both editing MCM values have been merged into one.

**Changed** SL01AmuletsSkyrim_CCOR_Patch.esp is now in LotD Patches Merge (following Lexy's)

**Changed** All Alternate Start options now have the difficulty before in the dialogue options to help you choose.

**Changed** You will no longer be able to persuade the Whiterun Guards so easily to get in.

**Changed** The vanilla start is no longer available due to it being extremely buggy.

## 0.9.5.5

**Added** HD Saw Dust

**Added** A new RaceMenu preset (Nord - Female)

---
**Updated** Legacy of the Dragonborn Patches (Official) (1.8.1 -> 1.8.2)

## 0.9.5.4

**Added** Salt and Wind - Rough Hair for KS Hairdo's SMP SE

---
**Updated** Interesting NPCs SE (4.3.6 -> 4.3.7)

**Updated** Salt and Wind - Rough hair for KS Hairdo's (3.0 -> 3.1)

**Updated** Legacy of the Dragonborn Patches SE (Official) (1.7 -> 1.8.1)

---
**Fixed** ReLinker Output not being included.

**Fixed** Dova-Hen meshes and textures not applying correctly

---
**Changed** Skyrim Souls now has paused book menus. (Reason: Me, Velexia and Erri had seemingly random CTDs on opening a book menu. This seemed to fix it for them.)

## 0.9.5.3
**Updated** I'm Walkin' Here (1.1.0 -> 1.2.1)

---
**Changed** Waiting speed has been increased by decreasing the factor to 0.23 (was 0.3)

---
**Fixed** The NeverNude profile should now actually be included.

**Fixed** Dark Brotherhood Sanctuary in Falkreath not using correct ELE lighting

**Fixed** Whiterun being gassed by the Stormcloaks (Green smoke previously visible in Whiterun when in exteriors)

## 0.9.5.2

**Fixed** Generated files are now hosted on the Wabbajack CDN server instead of MEGA. This should result in less errors from DynDOLOD Output / xLODGEN Output / TexGen Output. Thanks halgari!

**Fixed** Master audio levels being very low by default.

## 0.9.5.1

**Fixed** NeverNude profile not being included
                     
## 0.9.5 - The Creepy Creatures Update

**Added** CTD On Death Alas (Why? To prevent people from breaking their saves loading it over and over again, and make death a little scarier.)

**Added** The Falkreath Hauntings (Included in Animals Merged.esp)

**Added** The Falkreath Hauntings - Moon and Star Patch (Included in Animals Merged.esp)

**Added** Pandorable's Lucien Replacer

**Added** Bogmort - Mud Monsters of Morthal Swamp (Included in Animals Merged.esp)

**Added** 8K Night Skies - Stars and Galaxies - Cathedral Concept (High Realism)

**Added** Yggdrasil Music and SoundFX Overhaul SE

**Added** Nemesis - Project New Reign

**Added** The Ultimate Dodge Mod (automated MCM)

**Added** 4K SMIM Furniture Improvement

**Added** Rally's Farming Tools

**Added** Rally's Hooks and Saws

**Added** Rally's Nightingale Clutter

**Added** Rally's Jurgen Windcaller Tomb

**Added** Training Dummies Retexture 4k and 2k

**Added** Diverse Werewolves Collection SE

**Added** House Cats- Mihail Monsters and Animals

**Added** The Cat Mod (retexture by Pfuscher)

**Added** Squirrels- Mihail Monsters and Animals (SSE Port)

**Added** Volumetric Mists

**Added** 4K SMIM Furniture Improvement

**Added** Riekling Spears Revamped Texture (4K Brown Leather)

**Added** HD Lava 4K

**Added** Mushroom Retextures 2K

**Added** Realistic Skin and Hair Shaders - Giants

**Added** Realistic Skin Shaders - Falmer and Hagravens

**Added** Skyrim Horses Renewal SSE

**Added** Butterflies Unchained

**Added** Butterflies Unchained (CACO Patch)

**Added** Glorious Doors of Skyrim (GDOS) SE (with dark Whiterun door)

**Added** Serio's ENB

**Added** Project Dova-Hen SSE

**Added** Project Dova-Hen - Alternate Chicken (hey, that's mine!)

**Added** Realistic Husky Sounds SE

**Added** ElSopa - HD Keys SE

**Added** Argonians Enhanced (Eyes, Hairstyles, Mouth, Scars)

---
**Removed** Skyrim Immersive Creatures (Reason: This mod is broken beyond simple repair and was causing crashing issues)

**Removed** Skyrim Immersive Creatures - Official Patch and Patch Central

**Removed** 8K Night Skies - Stars and Galaxies - Cathedral Concept (Natural Fantasy) (Reason: replaced by High Realism)

**Removed** Fores New Idles in Skyrim (Reason: deprecated in favor of Nemesis, due to being FOSS software and the overall better solution)

**Removed** FNIS PCEA2

**Removed** Silent Horizons ENB (Reason: While a lovely ENB, the vivid visuals from this mod don't fit too well with the modlist.)

**Removed** Rally's Brooms - Dirty (Reason: replaced by Rally's Farming Tools, which contains this mod)

**Removed** TK Dodge (Reason: was replaced with The Ultimate Dodge Mod, which is more responsive/smoother)

---
**Updated** Septentrional Landscapes (1.2 -> 1.3.1)

**Updated** Fix note icon for SkyUI (SKSE64 plugin) (1.0 -> 1.2)

**Updated** Rallys Quill and Inkwell - SMIM version (1.0 -> 1.0, no version change but different file)

**Updated** Nether's Follower Framework (2.5.9 -> 2.6.0b)

**Updated** Storm Lightning for SSE and VR (Minty Lightning 2019) (1.4.3 -> 1.4.4)

**Updated** --JawZ-- Northern Lights Weather Add On for Cathedral Weathers (1.0a -> 1.1)

**Updated** Legacy of the Dragonborn SSE (5.0.29 -> 5.0.30)

---
**Changed** OMEGA LOTD Consistency Patch.esp (removed references to Immersive Creatures)

**Changed** LOTD Patches Merge Consistency Patch.esp (removed references to Immersive Creatures)

**Changed** Reinstalled OMEGA AIO with OBIS MLU Patch instead of SJs SIC and OBIS Patch (added to MLU Patches Merged.esp)

**Changed** Removed LVLI from MLU Consistency Patch.esp (removes SIC dependency)

**Changed** Removed NPC from SkyTEST Less Fear SSE Patch.esp (removes SIC dependency)

**Changed** Forwarded Skyrim Revamped - Complete Enemy Overhaul record to OMEGA Lexy LOTD Animals Merged Consistency Patch.esp except for DNAM - Player Skills on dunFolgunthur_MikrulGaulderson

**Changed** OMEGA CACO Patch for Immersive Creatures removed from OMEGA CACO Patches Merged.esp

**Changed** Enhanced Blood Textures no longer has SIC patch in Pre Bash Merged.esp

**Changed** You will now be able to switch profiles to NeverNude bodies.

---
**Fixed** Records not being properly forwarded in the Bashed Patch

**Fixed** Broken record NPC/DA02Champion in DBM_IW_Patch.esp (Legacy of the Dragonborn + Immersive Weapons patch)

**Fixed** Broken record LVLI/IWLIBestWarhammer in DBM_IW_Patch.esp (Legacy of the Dragonborn + Immersive Weapons patch)

**Fixed** Nimhe dying in Understone Keep (by removing Immersive Creatures)

**Fixed** Lots of left over Frostfall keywords in Lexy's OMEGA Conflict Resolution & trawzifiedsCustomResolution.esp have been removed

**Fixed** Lots of inconsistencies in trawzifiedsCustomResolution.esp

## 0.9.4

**Added** Smile in HD

**Added** More Wintersun Patches (Skyrim Immersive Creatures Patch)

**Added** Cave Roots 4K

**Added** Rally's Quill and Inkwell

**Added** Edwarrs Spell Tome Books - Converted with CAO

**Added** Recolored Edwarr's spell tomes (2K - Murky) - Converted with CAO

**Added** Less Sarcastic Applause - Converted with CAO

**Added** Fix Note icon for SkyUI (SKSE64 plugin)

**Added** CFTO - Winterhold Carriage (merged in CFTO - Merge.esp)

---
**Removed** Illustrious Whiterun (Reason: this is an amazing retexture, but it isn't quite finished yet. Waiting for some more polish, saw some issues with the Whiterun door handle, walls)

**Removed** Volcanic Tundra - Heat Wave Effects (Reason: mod already included in Realistic Water Two)

**Removed** Grass Addon for Veydosebrom Verdant and others (Reason: mod no longer works with Veydosebrom)

---
**Updated** SkyRem CORI (5.0.1 -> 5.0.2)

**Updated** Immersive Patrols (2.2.2 -> 2.2.3), also switched to the lite version without the Civil War Battles

**Updated** Northern Shores (1.2 -> 1.4)

**Updated** Blended Roads Redone (1.3 -> 1.4), switched to 4K version for consistency

**Updated** SkyRem EVE - Evolving Value Economy (1.0 -> 2.0)

**Updated** RS Children Overhaul (1.1.2 -> 1.1.3)

---
**Changed** Blended Roads Redone Bridges, switched to 4K version for consistency

**Changed** Septentrional Landscapes, switched to 4K version for consistency

**Changed** Carriage and Ferry Travel Overhaul is now merged

**Changed** Carriage and Ferry Travel Overhaul - Dawnstar by Arthmoor patch is now merged

---
**Fixed** Majestic Mountains not being included in the merge

**Fixed** Missing seq files for various merges

**Fixed** Added the custom resolution patch to the zEdit NPC Visual Transfer patcher

**Fixed** Duplicate merged LOTD Wintersun Patch. Now included in only LOTD Patches Merged.esp instead of the Wintersun Merge

**Fixed** Inn Room Costs MCM has been automated (removed MCM part in changelog)

**Fixed** Cathedral Weathers MCM has been automated (removed MCM part in changelog)

**Fixed** Carriages not showing costs

## 0.9.3

**Added** FemFeet Redesigned Fixes

**Added** FURtastic - Werebear Texture Overhaul (Grizzly)

**Added** Spellbreaker Oblivionized retexture in HD

**Added** 2K Sanguine Rose

**Added** Frankly HD Imperial Armor and Weapons (4K)

**Added** Frankly HD Imperial Armor and Weapons (LeanWolf's Better-Shaped Weapons Patch)

**Added** The Rueful Retexture - A Better Rueful Axe

**Added** Female Makeup Suite - Face - RaceMenu Overlays of Eyeliner EyeShadow Contours and Highlights - Special Edition

**Added** Skin Feature Overlays SE - Freckles Scars Birthmarks Stretch Marks Moles and More for Face and Body RaceMenu Overlays

**Added** Vanilla Makeup HD - HD Racial Colors and Makeup for all Races and Genders - SE

**Added** Improved Camera - INI Tweaks (used in the normal ini to make list not dependent of mod download, I've tweaked it a lot myself too)

**Added** --JawZ-- Northern Lights Weather Add On for Cathedral Weathers (in Weather Systems Merged.esp)

**Added** trawzifieds Weather Resolution Patch (in Weather Systems Merged.esp, makes JawZ & Obsidian Mountain Fogs work together)

---
**Removed** Seductive Lips SSE for UNP Body Renewal (Reason: No longer using UNP Body Renewal textures, and I think Bijin Skins' lips are just as good)

---
**Updated** Actor Limit Plugin (1.0 -> 2.0)

**Updated** Improved Rain and Fog for Cathedral Weathers (3.1 -> 3.2)

**Updated** HD Falmer and Chaurus with Glow - xTxVxTx (2.0 -> 2.5)

**Updated** Majestic Mountains (2.80 -> 2.82)

---
**Fixed** Silver Comb that could drop on noble NPCs being named as 'Gold' - thanks DwarfDude!

**Fixed** head parts being broken - hidden from RaceMenu for now because they don't seem to trigger a crash when used on NPCs (which is what they were meant for) - thanks Seriphia!

**Fixed** Alternate Conversation Camera and Improved Camera conflicting resulting in your head looking up to the sky/ground after exiting conversation

**Fixed** Small neck seam in some scenarios. Swapped to nude bodies by default. You can still easily switch to NeverNude for men and mer, covered in documentation now.

Moved the documentation to GitHub, updated with new MCM settings.

## 0.9.2
**Fixed** Serana running around without a head

**Fixed** Merges not being properly included in the Wabbajack installer

---
**Updated** SkyRem CORI (5.0.0 -> 5.0.1)
